﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class generateLevel {


	public static string[] Changing(int length, string day){
		string[] colors = new String[length];
		string daily;
		if(day.Equals(""))
			daily = System.DateTime.Today.GetHashCode().ToString();
		else
			daily = int.Parse(day).GetHashCode().ToString();

		if(daily[0] == '-')
			daily = daily.Substring(1, daily.GetHashCode().ToString().Length-1);
		else
			;

		Debug.Log(daily);
		while(daily.Length < length){
			if(daily.GetHashCode().ToString()[0] == '-')
				daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
			else
				daily += daily.GetHashCode().ToString();
		}
		// if(daily.Length > length){
		// 	daily = daily.Substring(0, length);
		// }
		int y = 0;
		for (int i = 0; i < length;y++)
		{
			if(y >= daily.Length-1){
				if(daily.GetHashCode().ToString()[0] == '-')
					daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
				else
					daily += daily.GetHashCode().ToString();
			}
			
			if(daily[y] == '7' || daily[y] == '8' || daily[y] == '9' || daily[y] == '0'){
				// y++;
				;
			}
			else{
				colors[i] = getColor(int.Parse(daily[y].ToString()));
				i++;
				// y++;
			}
		}
		
		return colors;
	}

	public static int[] ChangingRow(int length, string day, int Rows){
		int[] colors = new int[length];
		string daily;
		if(day.Equals(""))
			daily = System.DateTime.Today.GetHashCode().ToString();
		else
			daily = int.Parse(day).GetHashCode().ToString();
		if(daily[0] == '-')
			daily = daily.Substring(1, daily.GetHashCode().ToString().Length-1);
		else
			;
		Debug.Log(daily);
		while(daily.Length < length){
			if(daily.GetHashCode().ToString()[0] == '-')
				daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
			else
				daily += daily.GetHashCode().ToString();
		}
		// if(daily.Length > length){
		// 	daily = daily.Substring(0, length);
		// }
		int y = 0;
		char row = Rows.ToString()[0];
		for (int i = 0; i < length;y++)
		{
			if(y >= daily.Length-1)
			{
				if(daily.GetHashCode().ToString()[0] == '-')
					daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
				else
					daily += daily.GetHashCode().ToString();
			}
			
			if(row == '1'){
				if(daily[y] == '0'){
				colors[i] = int.Parse(daily[y].ToString());
				Debug.Log("iii " + colors[i]);
				i++;
				}
			}
			else if(row == '2'){
				if(daily[y] == '0' || daily[y] == '1'){
				colors[i] = int.Parse(daily[y].ToString());
				Debug.Log("iii " + colors[i]);
				i++;
				}
			}
			else{
				if(daily[y] == '0' || daily[y] == '1' || daily[y] == '2'){
				colors[i] = int.Parse(daily[y].ToString());
				Debug.Log("iii " + colors[i]);
				i++;
				}
			}
		}
		return colors;
	}

	public static string[] ColorRows(int rows, int length, string day){
		length = length*rows;
		string[] colors = new String[length];

		string daily;
		if(day.Equals(""))
			daily = System.DateTime.Today.GetHashCode().ToString();
		else
			daily = int.Parse(day).GetHashCode().ToString();

		if(daily[0] == '-')
			daily = daily.Substring(1, daily.GetHashCode().ToString().Length-1);
		else
			;
		while(daily.Length < length){
			if(daily.GetHashCode().ToString()[0] == '-')
				daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
			else
				daily += daily.GetHashCode().ToString();
		}
		int y = 0;
		for (int i = 0; i < length;y++)
		{
			// Debug.Log(daily);
			if(y >= daily.Length-1)
			{
				if(daily.GetHashCode().ToString()[0] == '-')
					daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
				else
					daily += daily.GetHashCode().ToString();
			}
			
			if(daily[y] == '7' || daily[y] == '8' || daily[y] == '9' || daily[y] == '0'){
				;
			}
			else{
				colors[i] = getColor(int.Parse(daily[y].ToString()));
				i++;
			}
		}
		
		return colors;
	}

	private static string getColor(int col1){
		string c1 = "";
		switch (col1)
		{
			case 1:
				c1 = "yellow";
				break;
			case 2:
				c1 = "red";
				break;
			case 3:
				c1 = "blue";
				break;
			case 4:
				c1 = "purple";
				break;
			case 5:
				c1 = "orange";
				break;
			case 6:
				c1 = "green";
				break;
			default:
				break;
		}
		return c1;
	}
}
