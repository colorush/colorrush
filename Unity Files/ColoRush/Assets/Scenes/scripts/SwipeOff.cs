﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwipeOff : MonoBehaviour
{
    public GameObject SideTime;

    public GameObject active;
    public GameObject deactive;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("SwipeOK") == 1)
            PlayerPrefs.SetFloat("SideTime",0);
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("SwipeOK") == 0){
            active.SetActive(true);
            deactive.SetActive(false);
            Button b = gameObject.GetComponent(typeof(Button)) as Button;
            ColorBlock colors = b.colors;
            colors.normalColor = Color.green;
            colors.highlightedColor = Color.green;
            b.colors = colors;
            }
        else{
            active.SetActive(false);
            deactive.SetActive(true);
            Button b = gameObject.GetComponent(typeof(Button)) as Button;
            ColorBlock colors = b.colors;
            colors.normalColor = Color.red;
            colors.highlightedColor = Color.red;
            b.colors = colors;
            }
        Debug.Log(PlayerPrefs.GetInt("SwipeOK"));
    }

    public void changeSwipe(){
        if(PlayerPrefs.GetInt("SwipeOK") == 0){
            PlayerPrefs.SetInt("SwipeOK",1);
            PlayerPrefs.SetFloat("SideTime",0);
            SideTime.GetComponent<SideTime>().side = 0;}
        else{
            PlayerPrefs.SetInt("SwipeOK",0);
            if(PlayerPrefs.GetFloat("SideTime") == 0){
                PlayerPrefs.SetFloat("SideTime",0.1f);
                SideTime.GetComponent<SideTime>().side = 0.1f;
            }
            }

    }
}
