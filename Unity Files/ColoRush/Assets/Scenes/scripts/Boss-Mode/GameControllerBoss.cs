﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerBoss : MonoBehaviour {
private int cloneNr = 1;
private GameObject clone;
private GameObject clone2;
private GameObject clone3;
private float speedBefore;
private float PauseSpeed;
private float time;
private int GemNr = 1;
private int row;
private int line3Counter;
private float smoothFactor;

public bool smooth;
private bool side = true;
public float changeTime2;
private float time2;
private bool PauseOrGO = false;
private float seconds2 = 0;

public GameObject item;
public GameObject itemLeft;
public float speed = 1;
public GameObject sphere;
public int score = 0;
public int HighScore = 0;
public Text scoreText; 
public Text HIText;
public Text GOText;
public Text PauseText;
public Text PauseGemText;
public GameObject GameOver;
public GameObject PauseButton;
public GameObject PauseObj;
// public AudioSource AS;
public Text timer;
public int Gems;
public int GemScore = 0;
public Text GemText;

public AudioSource[] Audios;
public int AudioFile;
public int SkinNum;

private bool dailyLevel;



	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("SpeedPref") > 0)
			speed = PlayerPrefs.GetFloat("SpeedPref");
		speed = speed + PlayerPrefs.GetFloat("SpeeDifference");
		if(speed < 0 )
			speed = 0.5f;
		changeTime2 = PlayerPrefs.GetFloat("SideTime");
		smoothFactor = PlayerPrefs.GetFloat("Smooth");
		if(PlayerPrefs.GetInt("SmoothOn") == 1)
			smooth = true;
		else
			smooth = false;
		row = 2;
		SkinNum = PlayerPrefs.GetInt("SkinNum");
		AudioFile = PlayerPrefs.GetInt("AudioFile");
		HighScore = PlayerPrefs.GetInt("HighScoreBoss");
		speedBefore = speed;
		Intialize(10);
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		Audio();
	}
	
	// Update is called once per frame
	void Update () {
		Audio();

		time += Time.deltaTime;
		PauseText.text = "" + score;
		PauseGemText.text = "" + GemScore;
		sphere.transform.position += new Vector3(0,0,Time.deltaTime * speed * 1);

		time2 += Time.deltaTime;
		seconds2 = time2 % 60;

		if(!PauseOrGO){
			timer.text = "" + seconds2;
			if(seconds2 <= changeTime2)
				side = false;
			else{
				side = true;}
		}

		if (score > HighScore)
		{
			HighScore =  score;
		}
		PlayerPrefs.SetInt("HighScoreBoss", HighScore);
		scoreText.text = score + " ";
		HIText.text = " HI " + HighScore;
		
		// Debug.Log((sphere.transform.position.z/5) + 15);
		if (cloneNr < (sphere.transform.position.z/5) + 11){
		generate(getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor());
		}
	}

	void OnApplicationPause(bool pauseStatus){
		if(pauseStatus){
			PauseObj.SetActive(true);
			PauseButton.SetActive(false);
			Pause();
		}
	}
	
	public void generate (string color1, string color2, string color1l, string color2l, string color1ll, string color2ll){
		clone = (GameObject)Instantiate (item, new Vector3(0,0,0), Quaternion.identity);
		clone.SetActive(true);
		clone.GetComponent<ItemBoss>().colorChange(color1, color2, cloneNr, cloneNr+1);
		clone.transform.position = new Vector3(0,0,cloneNr*5);
		clone.name = "clone" + " " + cloneNr;
		// clone.GetComponent<ItemBoss>().generateGems(Gems, GemNr);

		clone2 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
		clone2.SetActive(true);
		clone2.GetComponent<ItemBoss>().colorChange(color1l, color2l, 0, 0);
		clone2.transform.position = new Vector3(-2.6F,0,cloneNr*5);
		clone2.name = "clone" + " " + (cloneNr-1);
		clone2.GetComponent<ItemBoss>().generateGems(Gems, GemNr);

		clone3 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
		clone3.SetActive(true);
		clone3.GetComponent<ItemBoss>().colorChange(color1ll, color2ll, 0, 0);
		clone3.transform.position = new Vector3(-5.1F,0,cloneNr*5);
		clone3.name = "cloneT " + line3Counter;
		line3Counter++;

		
		GemNr += Gems;
		cloneNr += 2;
	}

	public void SphereColor(string color){
		time2 = 0;
		var seconds = time % 60;
		float changeTime = 0.5F;
		switch (color)
		{
			case "yellow":
				if(seconds <= changeTime){
					Debug.Log(seconds);
					sphere.GetComponent<SphereBoss>().doubleColor("yellow");
					time = 0;
				}
				else{
					sphere.GetComponent<SphereBoss>().color = "yellow";
				}
				break;
			case "blue":
				if(seconds <= changeTime){
					sphere.GetComponent<SphereBoss>().doubleColor("blue");
					time = 0;
				}
				else{
					sphere.GetComponent<SphereBoss>().color = "blue";
				}
				break;
			case "red":
				if(seconds <= changeTime){
					sphere.GetComponent<SphereBoss>().doubleColor("red");
					time = 0;
				}
				else{
					sphere.GetComponent<SphereBoss>().color = "red";
				}
				break;
			default:
			sphere.GetComponent<SphereBoss>().color = "white";
				break;
		}
		time = 0;
	}

	public void gameOver(){
		PauseOrGO = true;
		side = false;
		if(score > HighScore)
			HighScore = score;
		speed = 0;
		GameOver.SetActive(true);
		PauseButton.SetActive(false);
		GOText.text = "" + score;
		GemText.text = "" + GemScore;
		int gScore = PlayerPrefs.GetInt("Gems")+GemScore;
		PlayerPrefs.SetInt("Gems",gScore);
	}

	public void Reset(){
		side = true;
		PauseOrGO = false;
		GameOver.SetActive(false);
		score = 0;
		sphere.transform.position = new Vector3(0.02F, 0.5F, -22F);
		// SceneManager.LoadScene("Endless", LoadSceneMode.Single);
		DeleteBarriers();
		speed = speedBefore;
		speed = speed - PlayerPrefs.GetFloat("SpeeDifference");
		cloneNr = 1;
		line3Counter = 0;
		GemScore = 0;
		Start();
		SphereColor("white");
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
	}

	public void ScorePlus(){
		score++;
		if(score < 10){
			speed += PlayerPrefs.GetFloat("SpeedAddition");
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition");
		}
		else if(score < 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.1f;
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition") * 0.1f;
		}
		else if(score > 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.01f;
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition") * 0.01f;
		}
	}

	public void DeleteBarriers(){
		for(int i = 0; i <= cloneNr; i++){
			GameObject gb = GameObject.Find("clone " + i);
			Destroy(gb);
		}
		for (int i = 0; i <= line3Counter ; i++)
		{
			GameObject gb = GameObject.Find("cloneT " + i);
			Destroy(gb);
		}
		cloneNr = 0;
		for(int y = 0; y <= GemNr; y++){
			GameObject gb = GameObject.Find("gemT " + y);
			Destroy(gb);
		}
		GemNr = 0;
	}

	public void Pause(){
		PauseOrGO = true;
		side = false;
		PauseSpeed = speed;
		speed = 0;
	}

	public void GoOn(){
		PauseOrGO = false;
		side = true;
		speed = PauseSpeed;
	}

	public void Intialize(int Number){
		for(int i = 0; i < Number;i++){
		generate(getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor(), getRandomColor());
		}
	}

	public void swipe(string direction){
	if(side){
	if(smooth){
		switch (direction)
		{
			case "Left":
			if(row == 2){
				StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -2.6F));
				row = 1;
			}
			else if(row == 1){
				StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -5.1F));
				row = 0;
			}
				break;
			case "Right":
				if(row == 0){
				StartCoroutine (smooth_moveLeft ("Right", smoothFactor, -2.6F));
				row = 1;
				}
				else if(row == 1){
					StartCoroutine (smooth_moveLeft ("Right", smoothFactor, 0));
					row = 2;
				}
				break;
			default:
				break;
		}
		}
	else{
		switch (direction)
		{
			case "Left":
			if(row == 2){
				sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
				row = 1;
			}
			else if(row == 1){
				sphere.transform.position = new Vector3(-5.1f, sphere.transform.position.y, sphere.transform.position.z);
				row = 0;
			}
				break;
			case "Right":
				if(row == 0){
				sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
				row = 1;
				}
				else if(row == 1){
					sphere.transform.position = new Vector3(0, sphere.transform.position.y, sphere.transform.position.z);
					row = 2;
				}
				break;
			default:
				break;
		}
	}
	}
	}

	 IEnumerator smooth_moveLeft(string direct,float speed, float normal){
		 Vector3 direction;
         float startime = Time.time;
         Vector3 start_pos = sphere.transform.position; //Starting position.
         Vector3 end_pos; //Ending position.
		if(direct  == "Left"){
			direction  = Vector3.left;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x > normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
				if((sphere.transform.position += direction*move).x < normal)
					sphere.transform.position += direction*move;
	
				yield return null;
			}
		 }
		else{
			direction = Vector3.right;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x < normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				sphere.transform.position += direction*move;
	
				yield return null;
			}
		}
		 sphere.transform.position = new Vector3(normal, sphere.transform.position.y, sphere.transform.position.z);
     }


	public string getRandomColor(){
		int col1 = Random.Range(1,7);
		string c1 = "";
		switch (col1)
		{
			case 1:
				c1 = "yellow";
				break;
			case 2:
				c1 = "red";
				break;
			case 3:
				c1 = "blue";
				break;
			case 4:
				c1 = "purple";
				break;
			case 5:
				c1 = "orange";
				break;
			case 6:
				c1 = "green";
				break;
			default:
				break;
		}
		return c1;
	}

	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	public void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0)
			Audios[AudioFile].GetComponent<AudioSource>().mute = true;
		else
			Audios[AudioFile].GetComponent<AudioSource>().mute = false;
	}
	
	public void ButtonSide(){
		side = false;
	}
}
