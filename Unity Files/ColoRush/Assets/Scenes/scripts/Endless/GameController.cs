﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
private int cloneNr = 1;
private GameObject clone;
private float speedBefore;
private float PauseSpeed;

public GameObject item;
public float speed = 1;
public GameObject sphere;
public int score = 0;
public int HighScore = 0;
public Text scoreText; 
public Text HIText;
public Text GOText;
public Text PauseText;
public GameObject GameOver;
public GameObject PauseButton;
public GameObject PauseObj;

public AudioSource[] Audios;
public int AudioFile;
public int SkinNum;




	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("SpeedPref") > 0)
			speed = PlayerPrefs.GetFloat("SpeedPref");
		speed = speed + PlayerPrefs.GetFloat("SpeeDifference");
		if(speed < 0 )
			speed = 0.5f;
		SkinNum = PlayerPrefs.GetInt("SkinNum");
		AudioFile = PlayerPrefs.GetInt("AudioFile");
		HighScore = PlayerPrefs.GetInt("HighScoreEndless");
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		Audio();
		speedBefore = speed;
		Intialize(10);
	}
	
	// Update is called once per frame
	void Update () {
		Audio();
		PauseText.text = "" + score;
		sphere.transform.position += new Vector3(0,0,Time.deltaTime * speed * 1);

		if (score > HighScore)
		{
			HighScore =  score;
		}
		PlayerPrefs.SetInt("HighScoreEndless", HighScore);
		scoreText.text = score + " ";
		HIText.text = " HI " + HighScore;
		
		if (cloneNr < (sphere.transform.position.z/5) + 11){
		generate(getRandomColor(), getRandomColor());
	}
	}

	void OnApplicationPause(bool pauseStatus){
		if(pauseStatus){
			PauseObj.SetActive(true);
			PauseButton.SetActive(false);
			Pause();
		}
	}
	
	public void generate (string color1, string color2){
		clone = (GameObject)Instantiate (item, new Vector3(0,0,0), Quaternion.identity);
		clone.SetActive(true);
		clone.GetComponent<Item>().colorChange(color1, color2, cloneNr, cloneNr+1);
		clone.transform.position = new Vector3(0,0,cloneNr*5);
		clone.name = "clone" + " " + cloneNr;
		cloneNr += 2;
	}

	public void SphereColor(string color){
		switch (color)
		{
			case "yellow":
				sphere.GetComponent<SphereEndless>().color = "yellow";
				break;
			case "blue":
				sphere.GetComponent<SphereEndless>().color = "blue";
				break;
			case "red":
				sphere.GetComponent<SphereEndless>().color = "red";
				break;
			default:
				sphere.GetComponent<SphereEndless>().color = "white";
				break;
		}
	}

	public void gameOver(){
		if(score > HighScore)
			HighScore = score;
		speed = 0;
		GameOver.SetActive(true);
		PauseButton.SetActive(false);
		GOText.text = "" + score;
	}

	public void Reset(){
		GameOver.SetActive(false);
		score = 0;
		sphere.transform.position = new Vector3(0.02F, 0.5F, -22F);
		// SceneManager.LoadScene("Endless", LoadSceneMode.Single);
		DeleteBarriers();
		speed = speedBefore;
		speed = speed - PlayerPrefs.GetFloat("SpeeDifference");
		cloneNr = 1;
		Start();
		SphereColor("white");
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
	}

	public void ScorePlus(){
		score++;
		if(score < 10){
			speed += PlayerPrefs.GetFloat("SpeedAddition");
		}
		else if(score < 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.1f;
		}
		else if(score > 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.01f;
		}
	}

	public void DeleteBarriers(){
		for(int i = 1; i <= cloneNr; i += 2){
			GameObject gb = GameObject.Find("clone " + i);
			Destroy(gb);
		}
		cloneNr = 0;
	}

	public void Pause(){
		PauseSpeed = speed;
		speed = 0;
	}

	public void GoOn(){
		speed = PauseSpeed;
	}

	public string getRandomColor(){
		int col1 = Random.Range(1,4);
		string c1 = "";
		switch (col1)
		{
			case 1:
				c1 = "yellow";
				break;
			case 2:
				c1 = "red";
				break;
			case 3:
				c1 = "blue";
				break;
			default:
				break;
		}
		return c1;
	}

	public void Intialize(int Number){
		for(int i = 0; i < Number;i++){
		generate(getRandomColor(), getRandomColor());
		}
	}

	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	public void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0)
			Audios[AudioFile].GetComponent<AudioSource>().mute = true;
		else
			Audios[AudioFile].GetComponent<AudioSource>().mute = false;
	}
}
