﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skins : MonoBehaviour {
public GameObject[] gb;
public bool colorDefinitions;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void changeColor(string color){
		if(colorDefinitions){
			for(int i = 0; i < gb.Length;i++){
				gb[i].SetActive(false);
			}
			switch (color)
			{
				case "red":
					gb[0].SetActive(true);
					break;
				case "yellow":
					gb[1].SetActive(true);
					break;
				case "blue":
					gb[2].SetActive(true);
					break;
				case "purple":
					gb[3].SetActive(true);
					break;
				case "green":
					gb[4].SetActive(true);
					break;
				case "orange":
					gb[5].SetActive(true);
					break;
				default:
					gb[6].SetActive(true);
					break;
			}
		}
		else{
			for(int i = 0; i < gb.Length ;i++){
				Renderer s = gb[i].GetComponent<Renderer>();
				switch (color)
				{
					case "red":
						s.material.SetColor("_Color", Color.red);
						break;
					case "yellow":
						s.material.SetColor("_Color", Color.yellow);
						break;
					case "blue":
						s.material.SetColor("_Color", Color.blue);
						break;
					case "purple":
						s.material.SetColor("_Color", Color.magenta);
						break;
					case "green":
						s.material.SetColor("_Color", Color.green);
						break;
					case "orange":
						s.material.SetColor("_Color", new Color(0.89F,0.47F,0.15F));
						break;
					default:
						s.material.SetColor("_Color", Color.white);
						break;
				}
			}
		}
	}
}
