﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class leftRight : MonoBehaviour {
	public Text text;
	public GameObject Enabled;
	public bool choose;

	// Use this for initialization
	void Start () {
		if(choose){
			if(PlayerPrefs.GetInt("Arrows") == 1){
				text.fontSize = 350;
				if(this.name == "Left")
					text.text = "\u2190";
				else
					text.text = "\u2192";
			}
			else{
				text.fontSize = 200;
				if(this.name == "Left")
					text.text = "Links";
				else
					text.text = "Rechts";
			}
		}
		else{
			if(PlayerPrefs.GetInt("Arrows") == 1){
				text.fontSize = 250;
				if(this.name == "Left")
					text.text = "\u2190";
				else
					text.text = "\u2192";
			}
			else{
				text.fontSize = 100;
				if(this.name == "Left")
					text.text = "Links";
				else
					text.text = "Rechts";
			}
		}
	}
	
	// Update is called once per frame
	// void Update () {
	// 	if(Enabled != null){
	// 		if(PlayerPrefs.GetInt("Arrows") == 1)
	// 			Enabled.SetActive(true);
	// 		else
	// 			Enabled.SetActive(false);
	// 	}
	// }

	// public void Change(){
	// 	if(PlayerPrefs.GetInt("Arrows") == 1)
	// 		PlayerPrefs.SetInt("Arrows", 0);
	// 	else
	// 		PlayerPrefs.SetInt("Arrows", 1);
	// 	Debug.Log(PlayerPrefs.GetInt("Arrows"));
	// }
}
