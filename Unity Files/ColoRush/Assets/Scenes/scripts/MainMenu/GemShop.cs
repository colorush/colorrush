﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GemShop : MonoBehaviour {
public int[] price;
public int Gems;
public Text[] GemText;
public string[] costNames;
public GameObject[] TutorialObjects;
private bool case1 = false;

	private void Awake()
     {
         //Set screen size for Standalone
        #if UNITY_STANDALONE
                Screen.SetResolution(500,850,false);
                Screen.fullScreen = false;
        #endif

		// #if UNITY_WEBGL
		// 	Screen.SetResolution(500,850,false);
        //     Screen.fullScreen = false;
		// #endif
     }

	// Use this for initialization
	void Start () {
		Gems = PlayerPrefs.GetInt("Gems");
		// for (int i = 0; i < costNames.Length;i++){
		// 	if(PlayerPrefs.GetInt(costNames[i]) == 2)

		// }
	}
	
	// Update is called once per frame
	void Update () {
		
		if(PlayerPrefs.GetInt("Tutorial") == 0){
			PlayerPrefs.SetString("Scene","Tutorial");
			SceneManager.LoadScene("Loading");
		}
		Gems = PlayerPrefs.GetInt("Gems");
		for(int i = 0; i< GemText.Length;i++){
			GemText[i].text = "" + Gems;
		}
		if(PlayerPrefs.GetInt("Tutorial") == 1){
			TutorialStep(0);
		}
	}

	public bool buy(string name){
		switch (name)
		{
			case "Orbs":
			case "orbs2":
				if(Gems >= price[0]){
					PlayerPrefs.SetInt("Gems", Gems-price[0]);					
					return true;
				}
				else
				{
					return false;
				}
			case "Donut":
			case "SpaceLoveAttack":
			case "BossMusic":
			case "Boss":
			case "silvester":
			case "SkylineTwo":
			case "SkylineThree":
				if(Gems >= price[1]){
						PlayerPrefs.SetInt("Gems", Gems-price[1]);					
						return true;
					}
					else
					{
						return false;
					}
			case "Symbole":
			case "weapons":
			case "Aspire":
			case "Cookie":
				if(Gems >= price[2]){
						PlayerPrefs.SetInt("Gems", Gems-price[2]);					
						return true;
					}
					else
					{
						return false;
					}
			default:
				return false;
		}
	}

	public void TutorialStep(int nr){

		if(PlayerPrefs.GetInt("Tutorial") == 1){
		switch (nr)
		{
			case 0:
				if(case1 == false)
					TutorialStep(1);
				case1 = true;
				break;
			case 1:
				TutorialObjects[0].SetActive(true);
				break;
			case 2:
				TutorialObjects[0].SetActive(false);
				TutorialObjects[1].SetActive(true);
				break;
			case 3:
				TutorialObjects[1].SetActive(false);
				TutorialObjects[2].SetActive(true);
				break;
			case 4:
				TutorialObjects[2].SetActive(false);
				TutorialObjects[3].SetActive(true);
				break;
			case 5:
				TutorialObjects[3].SetActive(false);
				PlayerPrefs.SetInt("Tutorial", 2);
				break;
			default:
				break;
		}
	}
	}

	public void StartTutorial(){
		PlayerPrefs.SetInt("Tutorial", 0);
		
	}

	public void FPSChange(){
		if(PlayerPrefs.GetInt("FPS") == 1)
			PlayerPrefs.SetInt("FPS", 0);
		else
			PlayerPrefs.SetInt("FPS", 1);
	}
}
