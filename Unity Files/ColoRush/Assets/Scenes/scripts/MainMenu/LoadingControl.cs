﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingControl : MonoBehaviour {

public Slider slider;
AsyncOperation async;
public bool menu = false;

void Start(){
	LoadScene(PlayerPrefs.GetString("Scene"));
}

public void LoadScene(string scene){
	StartCoroutine(AsynchronousLoad(scene));
}

public void returnToMenu(){
	menu = true;
}

IEnumerator AsynchronousLoad (string scene)
{
	async = SceneManager.LoadSceneAsync(scene);
	async.allowSceneActivation = false;
	Debug.Log("start");
	
	while (async.isDone == false)
	{
		slider.value = async.progress;
		Debug.Log(async.progress);
		if (async.progress == 0.9f)
		{
			Debug.Log("finish");
			slider.value = 1f;
			// if(!menu)
				async.allowSceneActivation = true;
			// else
			// {
			// 	menu = false;
			// 	AsynchronousLoad("MainMenu");
			// }
		}
	
	yield return null;
	}
}
}
