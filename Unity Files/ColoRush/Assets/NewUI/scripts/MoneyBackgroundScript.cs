﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyBackgroundScript : MonoBehaviour
{
public GameObject GemShop;
public GameObject Lock;
public int SkinNr;
public GameObject[] disable;
public GameObject Main;
public GameObject leftDown;
public GameObject rightDown;
public GameObject leftUp;
public GameObject rightUp;
public bool Onleft;
public bool down;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt(name) == 1){
			Lock.SetActive(false);
		}

        int skin = PlayerPrefs.GetInt("Backgrounds");;
		if(skin == SkinNr){
			if(Onleft && down){
				leftDown.SetActive(true);
				}
			else if(down){
				rightDown.SetActive(true);
			}
			else if(Onleft){
				leftUp.SetActive(true);
			}
			else{
				rightUp.SetActive(true);
				}
		}
		else{
			if(Onleft && down){
				leftDown.SetActive(false);
				}
			else if(down){
				rightDown.SetActive(false);
			}
			else if(Onleft){
				leftUp.SetActive(false);
			}
			else{
				rightUp.SetActive(false);
				}
		}
    }

    public void clickBuy(){
		if(GemShop.GetComponent<GemShop>().buy(name)){
			PlayerPrefs.SetInt(name, 1);
			 PlayerPrefs.SetInt("Backgrounds", SkinNr);
			Close();
		}
	}
	
	public void SetNrMoney(){
		if(PlayerPrefs.GetInt(name) == 1){
			 PlayerPrefs.SetInt("Backgrounds", SkinNr);
			Close();
		}
		else{
			clickBuy();
		}
	}

	private void Close(){
		for(int i = 0; i < disable.Length;i++){
			disable[i].SetActive(false);
		}
		Main.SetActive(true);
	}
}
