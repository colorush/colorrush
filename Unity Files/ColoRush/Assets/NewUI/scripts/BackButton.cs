﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackButton : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Button b = gameObject.GetComponent(typeof(Button)) as Button;
        if (Input.GetKeyDown(KeyCode.Escape)){
            b.onClick.Invoke();
        }
    }
}
