﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {
public string[] levels;
public GameObject[] finished;
public GameObject[] LockObj;
public EditorControl EC;
public EditorControl EC2;
bool MultiplayerModes;
	// Use this for initialization
	void Start () {
		// PlayerPrefs.DeleteAll();
		check();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Multi(){
		MultiplayerModes = true;
	}

	public void MakeMode (string HashCode, int Gems) {
		//HashCode length should be 8!
		//length is first, second, third and fourth char
		//GemAward is fifth and sixth char
		//ManyRows is seventh char
		//Mode is eigth char
		//Level is nineth and tenth char

		string daily = HashCode;

		if(daily[0] == '-')
			daily = daily.Substring(1, daily.GetHashCode().ToString().Length-1);
		else
			;

		Debug.Log(daily);
		Debug.Log(daily.GetHashCode().ToString());

		// public bool ColorButtons;
		// public bool Running;
		// public int ManyRows;
		// public int length;
		// public int GemAward;

		while(daily.Length < 8){
			if(daily.GetHashCode().ToString()[0] == '-')
				daily += daily.GetHashCode().ToString().Substring(1, daily.GetHashCode().ToString().Length-1);
			else
				daily += daily.GetHashCode().ToString();
		}

		int length = int.Parse("" + daily[0] + daily[1] + daily[2] + daily[3]);
		
		Debug.Log("leng " + length);
		
		int GemAward;
		
		if(Gems > 0){
			GemAward = int.Parse("" + daily[4] + daily[5]);
			Debug.Log("gemsValue " + GemAward);

		}
		else{
			GemAward = Gems;
		}

		// bool ColorButtons = false;
		// bool Running = false;
		int Mode = 0;
		if(int.Parse("" + daily[7]) == 2)
			Mode = 2;
		else if(int.Parse("" + daily[7]) == 1)
			Mode = 1;
		else if(MultiplayerModes && int.Parse("" + daily[7]) == 3){
			Mode = 4;
		}
		else if(MultiplayerModes && int.Parse("" + daily[7]) == 4){
			Mode = 3;
		}
		else if(int.Parse("" + daily[7]) >= 6)
			Mode = 2;
		else
			Mode = 1;

		// int ManyRows;
		// if(ColorButtons){
		// 	int lol = int.Parse(daily[6].ToString());
		// 	if(lol <= 4)
		// 		ManyRows = 3;
		// 	else if(lol <= 7)
		// 		ManyRows = 2;
		// 	else
		// 		ManyRows = 1;
		// }
		// else
		// 	ManyRows = 3;

		PlayerPrefs.SetInt("LevelManyRows", int.Parse(daily[6].ToString()));
		Debug.Log("gems " + GemAward);	
		PlayerPrefs.SetInt("LevelGemAward", GemAward);
		Debug.Log("leng " + length);
		PlayerPrefs.SetInt("LevelLength", length);
		// if(Running)
		PlayerPrefs.SetInt("LevelRun",Mode);
		// else
		// 	PlayerPrefs.SetInt("LevelRun",2);

		PlayerPrefs.SetString("LevelHash", HashCode);

		// PlayerPrefs.SetInt("LevelNum",  "" + daily[8] + daily[9]);

	}

	public void SelectLevel(int level){
		//HashCode length should be 8!
		//length is first, second, third and fourth char
		//GemAward is fifth and sixth char
		//ManyRows is seventh char
		//Mode is eigth char
		//Level is nineth and tenth char

		// if (checkLevel(level)){
			if(PlayerPrefs.GetInt("Level"+level) == 1)
				MakeMode(levels[level],0);
			else
				MakeMode(levels[level],1);
			PlayerPrefs.SetInt("LevelNum",level);
		// }
	}

	public void SelectMultiLevel(int level){
		if (checkLevel(level)){
			if(PlayerPrefs.GetInt("Level"+level) == 1)
				MakeMode(levels[level],0);
			else
				MakeMode(levels[level],1);
			PlayerPrefs.SetInt("LevelNum",level);
			EC.SendLevelData(PlayerPrefs.GetString("LevelHash"));
		}
	}

	public void nextMultiLevel(){
		if(PlayerPrefs.GetInt("LevelNum") >= 21)
			;
		else
			SelectMultiLevelECTwo(PlayerPrefs.GetInt("LevelNum")+1);
	}
	void SelectMultiLevelECTwo(int level){
		if (checkLevel(level)){
			if(PlayerPrefs.GetInt("Level"+level) == 1)
				MakeMode(levels[level],0);
			else
				MakeMode(levels[level],1);
			PlayerPrefs.SetInt("LevelNum",level);
			EC2.SendLevelData(PlayerPrefs.GetString("LevelHash"));
		}
	}

	/* Documentation:
	length: first four characters as number in one row
	GemAward: next two characters as number in one row
	ManyRows: next number as number of rows (1 = one row, 2 = two rows, etc.)
	Mode: 1 = ColorButtons, 2 = Running
	*/

	bool checkLevel(int nr){
		for (int i = 1; i < nr; i++)
		{
			if(PlayerPrefs.GetInt("Level"+i) == 0)
				return false;
		}
		return true;
	}

	void check(){
		for (int i = 1; i < levels.Length; i++)
		{
			finished[i].SetActive(false);
		}
		for (int i = 1; i < levels.Length; i++)
		{
			if(PlayerPrefs.GetInt("Level"+i) == 1)
				finished[i].SetActive(true);
		}
		for (int i = 1; i < levels.Length; i++)
		{
			LockObj[i].SetActive(false);
		}
		for (int i = 1; i < levels.Length-1; i++)
		{
			if(PlayerPrefs.GetInt("Level"+i) == 0)
				LockObj[i+1].SetActive(true);
			// LockObj[i].SetActive(false);	
		}
	}

	void TurnLock(int nr){
		for (int i = 1; i < levels.Length; i++)
		{
			LockObj[i].SetActive(false);
		}
		for (int i = nr; i < levels.Length; i++)
		{
			LockObj[i].SetActive(true);
		}
	}
}