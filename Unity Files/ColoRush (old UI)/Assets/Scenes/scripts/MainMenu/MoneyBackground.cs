﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyBackground : MonoBehaviour

{
public GameObject GemShop;
public GameObject Lock;
public GameObject[] disable;
public GameObject Main;
int SkinNr;
public int bgNR;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SkinNr = bgNR;
        if(PlayerPrefs.GetInt(name) == 1){
			Lock.SetActive(false);
		}
    }

    public void clickBuy(){
		if(GemShop.GetComponent<GemShop>().buy(name)){
			PlayerPrefs.SetInt(name, 1);
			 PlayerPrefs.SetInt("Backgrounds", bgNR);
			Close();
		}
	}
	
	public void SetNrMoney(){
		if(PlayerPrefs.GetInt(name) == 1){
			 PlayerPrefs.SetInt("Backgrounds", bgNR);
			Close();
		}
		else{
			clickBuy();
		}
	}

	private void Close(){
		for(int i = 0; i < disable.Length;i++){
			disable[i].SetActive(false);
		}
		Main.SetActive(true);
	}
}
