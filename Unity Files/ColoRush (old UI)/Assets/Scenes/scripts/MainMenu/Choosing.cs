﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Choosing : MonoBehaviour {

public int SkinNr;
public GameObject[] disable;
public GameObject Main;
public GameObject page1;

public GameObject leftDown;
public GameObject rightDown;
public GameObject leftUp;
public GameObject rightUp;
public bool Onleft;
public bool down;


	// Use this for initialization
	void Start () {
		// Button = gameObject.GetComponent<Button>();
	}
	
	// Update is called once per frame
	void Update (){
		int skin = PlayerPrefs.GetInt("SkinNum");
		if(skin == SkinNr){
			if(Onleft && down){
				leftDown.SetActive(true);
				}
			else if(down){
				rightDown.SetActive(true);
			}
			else if(Onleft){
				leftUp.SetActive(true);
			}
			else{
				rightUp.SetActive(true);
				}
		}
		else{
			if(Onleft && down){
				leftDown.SetActive(false);
				}
			else if(down){
				rightDown.SetActive(false);
			}
			else if(Onleft){
				leftUp.SetActive(false);
			}
			else{
				rightUp.SetActive(false);
				}
		}
		
	}

	public void SetNr(){
		PlayerPrefs.SetInt("SkinNum", SkinNr);
		Close();
	}

	private void Close(){
		page1.SetActive(true);
		for(int i = 0; i < disable.Length;i++){
			disable[i].SetActive(false);
		}
		Main.SetActive(true);

	}
}
