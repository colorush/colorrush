﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeedControl : MonoBehaviour
{
public float speedChange;
public Slider LengSlider;
public Text LengActual;
public InputField LengInputField;
bool LengFocused = false;

public float speedManual;
public Slider ManualSlider;
public Text ManualActual;
public InputField ManualInputField;
bool ManualFocused = false;

public float speedAddition;
public Slider AdditionSlider;
public Text AdditionActual;
public InputField AdditionInputField;
bool AdditionFocused = false;
public bool Multi;

    // Start is called before the first frame update
    void Start()
    {
        speedChange = PlayerPrefs.GetFloat("SpeeDifference");
		LengSlider.value = speedChange;
		LengInputField.text = "" + speedChange;

		speedManual = PlayerPrefs.GetFloat("SpeedPref");
		ManualSlider.value = speedManual;
		ManualInputField.text = "" + speedManual;

		speedAddition = PlayerPrefs.GetFloat("SpeedAddition");
		AdditionSlider.value = speedAddition;
		AdditionInputField.text = "" + speedAddition;
    }

    // Update is called once per frame
    void Update()
    {
		if(Multi)
			PlayerPrefs.SetFloat("MultiSpeeDifference", speedChange);
		else
        	PlayerPrefs.SetFloat("SpeeDifference", speedChange);
		
		//SpeeDifference
		LengSlider.value = speedChange;
		if(!LengFocused)
			LengInputField.text = "" + speedChange;
		if (LengInputField.isFocused){
			LengFocused = true;
		}
		if(!LengInputField.isFocused && LengFocused){
			LengChangeValue();
		}
		LengActual.text = "" + speedChange;

		//manualSpeed
		if(Multi)
			PlayerPrefs.SetFloat("MultiSpeedPref", speedManual);
		else
			PlayerPrefs.SetFloat("SpeedPref", speedManual);
		
		ManualSlider.value = speedManual;
		if(!ManualFocused)
			ManualInputField.text = "" + speedManual;
		if (ManualInputField.isFocused){
			ManualFocused = true;
		}
		if(!ManualInputField.isFocused && ManualFocused){
			ManualChangeValue();
		}
		ManualActual.text = "" + speedManual;

		//additionalSpeed
		if(Multi)
			PlayerPrefs.SetFloat("MultiSpeedAddition", speedAddition);
		else
			PlayerPrefs.SetFloat("SpeedAddition", speedAddition);
		
		AdditionSlider.value = speedAddition;
		if(!AdditionFocused)
			AdditionInputField.text = "" + speedAddition;
		if (AdditionInputField.isFocused){
			AdditionFocused = true;
		}
		if(!AdditionInputField.isFocused && AdditionFocused){
			AdditionChangeValue();
		}
		AdditionActual.text = "" + speedAddition;
    }

    void LengChangeValue(){
		speedChange = float.Parse(LengInputField.text);
		LengFocused = false;
	}

    public void LengValueChange(){
		speedChange = (float) LengSlider.value;
	}

	void ManualChangeValue(){
		speedManual = float.Parse(ManualInputField.text);
		ManualFocused = false;
	}

    public void ManualValueChange(){
		speedManual = (float) ManualSlider.value;
	}

	void AdditionChangeValue(){
		speedAddition = float.Parse(AdditionInputField.text);
		AdditionFocused = false;
	}

    public void AdditionValueChange(){
		speedAddition = (float) AdditionSlider.value;
	}
}
