﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierProf : MonoBehaviour {
public GameObject sphere;
public GameObject GameController;
public bool tutorial;
// private GameObject Parent;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "sphere"){
			if(tutorial){
				if(this.name == sphere.GetComponent<TutorialSphere>().color){
					Debug.Log("destroy... " + name);
					GameController.GetComponent<TutorialGameController>().ScorePlus();
					// this.transform.parent.GetComponent<Under>().Call();
					// GameObject P = this.transform.parent.gameObject;
					// P.GetComponent<Under>().Call();
					// MyParentScript myScript = this.GetComponentInParent();
					// myScript.Call()

					Destroy(this);
				}
				else
					GameController.GetComponent<TutorialGameController>().gameOver();
			}
			else{
				if(this.name == sphere.GetComponent<Sphere>().color){
					Debug.Log("destroy... " + name);
					GameController.GetComponent<GameControllerProf>().ScorePlus();
					// this.transform.parent.GetComponent<Under>().Call();
					// GameObject P = this.transform.parent.gameObject;
					// P.GetComponent<Under>().Call();
					// MyParentScript myScript = this.GetComponentInParent();
					// myScript.Call()

					Destroy(this);
			}
			else
				GameController.GetComponent<GameControllerProf>().gameOver();
			}
		}
	}
	}

