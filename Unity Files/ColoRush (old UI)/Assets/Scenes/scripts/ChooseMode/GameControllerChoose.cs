﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameControllerChoose : MonoBehaviour {
private int cloneNr = 1;
private GameObject clone;
private GameObject clone2;
private GameObject clone3;
private float speedBefore;
private float PauseSpeed;
private float time;
private int GemNr = 0;
public int row;
private int line3Counter;
private string[] colorArray = new string[10000];
private int barrierNum = 0;
private int randBefore = 0;
private float smoothFactor;

private bool side = true;
public bool smooth;

public bool changing;
public bool hard;

public GameObject item;
public GameObject itemLeft;
public float speed = 1;
public GameObject sphere;
public int score = 0;
public int HighScore = 0;
public Text scoreText; 
public Text HIText;
public Text GOText;
public Text PauseText;
public Text PauseGemText;
public GameObject GameOver;
// public AudioSource AS;
public Text timer;
public int Gems;
public int GemScore = 0;
public Text GemText;
public string StandardColor;

public AudioSource[] Audios;
public int AudioFile;
public int SkinNum;



	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("SpeedPref") > 0)
			speed = PlayerPrefs.GetFloat("SpeedPref");
		speed = speed + PlayerPrefs.GetFloat("SpeeDifference");
		if(speed < 0 )
			speed = 0.5f;
		smoothFactor = PlayerPrefs.GetFloat("Smooth");
		if(PlayerPrefs.GetInt("SmoothOn") == 1)
			smooth = true;
		else
			smooth = false;
		if (PlayerPrefs.GetInt("hard")==1)
			hard = true;
		else
			hard = false;
		StandardColor = getRandomColor();
		row = 2;
		SkinNum = PlayerPrefs.GetInt("SkinNum");
		AudioFile = PlayerPrefs.GetInt("AudioFile");
		HighScore = PlayerPrefs.GetInt("HighScoreTriple");
		speedBefore = speed;
		if(changing){
			HighScore = PlayerPrefs.GetInt("HighScoreChange");
				for(int i = 1; i < colorArray.Length;i++){
				colorArray[i] = getRandomColor();
			}
			colorArray[0] = StandardColor;
			Debug.Log("test " + colorArray[0]);
			}
		if(hard == false){
			randBefore = 5;
			speed = 2.5F;
			speedBefore = speed;
		}
		else
			HighScore = PlayerPrefs.GetInt("HighScoreChangeHard");
		Intialize(10);
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		Audio();
		SphereColor(StandardColor);
		
	}
	
	// Update is called once per frame
	void Update () {
		Audio();
		SphereColor(StandardColor);

		time += Time.deltaTime;
		PauseText.text = "" + score;
		PauseGemText.text = "" + GemScore;
		sphere.transform.position += new Vector3(0,0,Time.deltaTime * speed);


		if (score > HighScore)
		{
			HighScore =  score;
		}
		if(changing){
			if(hard)
				PlayerPrefs.SetInt("HighScoreChangeHard", HighScore);
			else
				PlayerPrefs.SetInt("HighScoreChange", HighScore);
			}
		else
			PlayerPrefs.SetInt("HighScoreTriple", HighScore);
		scoreText.text = score + " ";
		HIText.text = " HI " + HighScore;
		
		if (cloneNr < (sphere.transform.position.z/5) + 11){
			generateRandColor();
		}
	}
	
	public void generate (string color1, string color2, string color1l, string color2l, string color1ll, string color2ll, int row1, int row2, string standColor, string standColor2){
		clone = (GameObject)Instantiate (item, new Vector3(0,0,0), Quaternion.identity);
		clone.SetActive(true);
		if(row1 == 0){
			if(row2 == 0)
				clone.GetComponent<ItemBoss>().colorChange(standColor, standColor2, cloneNr, cloneNr+1);
			else
				clone.GetComponent<ItemBoss>().colorChange(standColor, color2, cloneNr, cloneNr+1);
			}
		else if(row2 == 0){
			clone.GetComponent<ItemBoss>().colorChange(color1, standColor2, cloneNr, cloneNr+1);
		}
		else
			clone.GetComponent<ItemBoss>().colorChange(color1, color2, cloneNr, cloneNr+1);
		clone.transform.position = new Vector3(0,0,cloneNr*5);
		clone.name = "clone" + " " + cloneNr;
		// clone.GetComponent<ItemBoss>().generateGems(Gems, GemNr);

		clone2 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
		clone2.SetActive(true);
		if(row1 == 1){
			if(row2 == 1)
				clone2.GetComponent<ItemBoss>().colorChange(standColor, standColor2, 0, 0);
			else
				clone2.GetComponent<ItemBoss>().colorChange(standColor, color2l, 0, 0);
			}
		else if(row2 == 1){
			clone2.GetComponent<ItemBoss>().colorChange(color1l, standColor2, 0, 0);
		}
		else
			clone2.GetComponent<ItemBoss>().colorChange(color1l, color2l, 0, 0);
		clone2.transform.position = new Vector3(-2.6F,0,cloneNr*5);
		clone2.name = "clone" + " " + (cloneNr-1);
		clone2.GetComponent<ItemBoss>().generateGems(Gems, GemNr);

		clone3 = (GameObject)Instantiate (itemLeft, new Vector3(0,0,0), Quaternion.identity);
		clone3.SetActive(true);
		if(row1 == 2){
			if(row2 == 2)
				clone3.GetComponent<ItemBoss>().colorChange(standColor, standColor2, 0, 0);
			else
				clone3.GetComponent<ItemBoss>().colorChange(standColor, color2ll, 0, 0);
			}
		else if(row2 == 2){
			clone3.GetComponent<ItemBoss>().colorChange(color1ll, standColor2, 0, 0);
		}
		else
			clone3.GetComponent<ItemBoss>().colorChange(color1ll, color2ll, 0, 0);
		clone3.transform.position = new Vector3(-5.1F,0,cloneNr*5);
		clone3.name = "cloneT " + line3Counter;
		line3Counter++;

		
		GemNr+= Gems;
		cloneNr += 2;
	}

	public void SphereColor(string color){
		var seconds = time % 60;
		timer.text = "" + seconds;
		switch (color)
		{
			case "yellow":
				sphere.GetComponent<SphereBoss>().color = "yellow";
				break;
			case "red":
				sphere.GetComponent<SphereBoss>().color = "red";
				break;
			case "blue":
				sphere.GetComponent<SphereBoss>().color = "blue";
				break;
			case "orange":
				sphere.GetComponent<SphereBoss>().color = "orange";
				break;
			case "purple":
				sphere.GetComponent<SphereBoss>().color = "purple";
				break;
			case "green":
				sphere.GetComponent<SphereBoss>().color = "green";
				break;
			default:
			sphere.GetComponent<SphereBoss>().color = "white";
				break;
		}
		time = 0;
	}

	public void gameOver(){
		side = false;
		if(score > HighScore)
			HighScore = score;
		speed = 0;
		GameOver.SetActive(true);
		GOText.text = "" + score;
		GemText.text = "" + GemScore;
		int gScore = PlayerPrefs.GetInt("Gems")+GemScore;
		PlayerPrefs.SetInt("Gems",gScore);
	}

	public void Reset(){
		if(changing){
			SceneManager.LoadScene("Triple-Change", LoadSceneMode.Single);
		}
		else{
		GameOver.SetActive(false);
		side = true;
		score = 0;
		sphere.transform.position = new Vector3(0.02F, 0.5F, -26F);
		row = 2;
		DeleteBarriers();
		speed = speedBefore;
		speed = speed - PlayerPrefs.GetFloat("SpeeDifference");
		cloneNr = 1;
		line3Counter = 0;
		GemScore = 0;
		// StandardColor = getRandomColor();
		// colorArray = new string[10000];
		Start();
		Audios[AudioFile].GetComponent<AudioSource>().Play(0);
		}
	}

	public void ScorePlus(){
		barrierNum++;
		if(changing)
			StandardColor = colorArray[barrierNum];
		score++;
		if(score < 10){
			speed += PlayerPrefs.GetFloat("SpeedAddition");
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition");
		}
		else if(score < 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.1f;
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition") * 0.1f;
		}
		else if(score > 100){
			speed += PlayerPrefs.GetFloat("SpeedAddition") * 0.01f;
			smoothFactor += PlayerPrefs.GetFloat("SpeedAddition") * 0.01f;
		}
	}

	public void DeleteBarriers(){
		for(int i = 0; i <= cloneNr; i++){
			GameObject gb = GameObject.Find("clone " + i);
			Destroy(gb);
		}
		for (int i = 0; i <= line3Counter ; i++)
		{
			GameObject gb = GameObject.Find("cloneT " + i);
			Destroy(gb);
		}
		cloneNr = 0;
		for(int y = 0; y <= GemNr; y++){
			GameObject gb = GameObject.Find("gemT " + y);
			Destroy(gb);
		}
		GemNr = 0;
	}

	public void Pause(){
		side = false;
		PauseSpeed = speed;
		speed = 0;
	}

	public void GoOn(){
		side  = true;
		speed = PauseSpeed;
	}

	public void Intialize(int Number){
		for(int i = 0; i < Number;i++){
		generateRandColor();
		}
	}

	public void swipe(string direction){
	if(side){
		if(smooth){
		switch (direction)
		{
			case "Left":
			if(row == 2){
				StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -2.6F));
				row = 1;
			}
			else if(row == 1){
				StartCoroutine (smooth_moveLeft ("Left", smoothFactor, -5.1F));
				row = 0;
			}
				break;
			case "Right":
				if(row == 0){
				StartCoroutine (smooth_moveLeft ("Right", smoothFactor, -2.6F));
				row = 1;
				}
				else if(row == 1){
					StartCoroutine (smooth_moveLeft ("Right", smoothFactor, 0));
					row = 2;
				}
				break;
			default:
				break;
		}
		}
	else{
		switch (direction)
		{
			case "Left":
			if(row == 2){
				sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
				row = 1;
			}
			else if(row == 1){
				sphere.transform.position = new Vector3(-5.1f, sphere.transform.position.y, sphere.transform.position.z);
				row = 0;
			}
				break;
			case "Right":
				if(row == 0){
				sphere.transform.position = new Vector3(-2.6f, sphere.transform.position.y, sphere.transform.position.z);
				row = 1;
				}
				else if(row == 1){
					sphere.transform.position = new Vector3(0, sphere.transform.position.y, sphere.transform.position.z);
					row = 2;
				}
				break;
			default:
				break;
		}
	}
	}
	}

	 IEnumerator smooth_moveLeft(string direct,float speed, float normal){
		 Vector3 direction;
         float startime = Time.time;
         Vector3 start_pos = sphere.transform.position; //Starting position.
         Vector3 end_pos; //Ending position.
		if(direct  == "Left"){
			direction  = Vector3.left;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x > normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				sphere.transform.position += direction*move;
	
				yield return null;
			}
		 }
		else{
			direction = Vector3.right;
			end_pos = sphere.transform.position + direction;
			while (start_pos != end_pos && ((Time.time - startime)*speed) < 1f && sphere.transform.position.x < normal) { 
				float move = Mathf.Lerp (0,1, (Time.time - startime)*speed);
	
				sphere.transform.position += direction*move;
	
				yield return null;
			}
		}
		 sphere.transform.position = new Vector3(normal, sphere.transform.position.y, sphere.transform.position.z);
     }
	public string getRandomColor(){
		int col1 = Random.Range(1,7);
		string c1 = "";
		switch (col1)
		{
			case 1:
				c1 = "yellow";
				break;
			case 2:
				c1 = "red";
				break;
			case 3:
				c1 = "blue";
				break;
			case 4:
				c1 = "purple";
				break;
			case 5:
				c1 = "orange";
				break;
			case 6:
				c1 = "green";
				break;
			default:
				break;
		}
		return c1;
	}

	public void Mute(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol - 1 == 0)
			vol = 0;
		else
			vol = 1;
		PlayerPrefs.SetInt("Volume", vol);
	}

	public void Audio(){
		int vol = PlayerPrefs.GetInt("Volume");
		if(vol == 0)
			Audios[AudioFile].GetComponent<AudioSource>().mute = true;
		else
			Audios[AudioFile].GetComponent<AudioSource>().mute = false;
	}

	public void generateRandColor(){
		int rand1;
		int rand2;
		rand1 = Random.Range(0,3);
		rand2 = Random.Range(0,3);
		if(hard){
			while(rand1 == randBefore)
				rand1 = Random.Range(0,3);
			while(rand2 == rand1){
				rand2 = Random.Range(0,3);
				}
			randBefore = rand2;
		}


		if(changing){
			string coloR1 = getRandomColor();
			while(coloR1 == colorArray[cloneNr-1])
				coloR1 = getRandomColor();
			string coloR2 = getRandomColor();
			while(coloR2 == colorArray[cloneNr])
				coloR2 = getRandomColor();
			string coloR1l = getRandomColor();
			while(coloR1l == colorArray[cloneNr-1])
				coloR1l = getRandomColor();
			string coloR2l = getRandomColor();
			while(coloR2l == colorArray[cloneNr])
				coloR2l = getRandomColor();
			string coloR1ll = getRandomColor();
			while(coloR1ll == colorArray[cloneNr-1])
				coloR1ll = getRandomColor();
			string coloR2ll = getRandomColor();
			while(coloR2ll == colorArray[cloneNr])
				coloR2ll = getRandomColor();
			generate(coloR1, coloR2, coloR1l, coloR2l, coloR1ll, coloR2ll, rand1, rand2, colorArray[cloneNr-1], colorArray[cloneNr]);
			Debug.Log("color1 " + colorArray[cloneNr-1]);

			

		}
		else{
			string coloR1 = getRandomColor();
			while(coloR1 == StandardColor)
				coloR1 = getRandomColor();
			string coloR2 = getRandomColor();
			while(coloR2 == StandardColor)
				coloR2 = getRandomColor();
			string coloR1l = getRandomColor();
			while(coloR1l == StandardColor)
				coloR1l = getRandomColor();
			string coloR2l = getRandomColor();
			while(coloR2l == StandardColor)
				coloR2l = getRandomColor();
			string coloR1ll = getRandomColor();
			while(coloR1ll == StandardColor)
				coloR1ll = getRandomColor();
			string coloR2ll = getRandomColor();
			while(coloR2ll == StandardColor)
				coloR2ll = getRandomColor();
			generate(coloR1, coloR2, coloR1l, coloR2l, coloR1ll, coloR2ll, rand1, rand2, StandardColor, StandardColor);
	}
	}
	
	public int returnScore(){
		return score;
	}

	public void HardMode(bool ha){
		hard = ha;
		if(ha)
			PlayerPrefs.SetInt("hard", 1);
		else
			PlayerPrefs.SetInt("hard", 0);
		PlayerPrefs.SetInt("chosen", 1);
		Debug.Log(PlayerPrefs.GetInt("chosen"));
	}

}
