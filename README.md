# ColoRush

[<img src="https://raw.githubusercontent.com/LibreShift/red-moon/master/art/direct-apk-download.png"
      alt="Direct apk download"
      height="130">](https://gitlab.com/colorush/colorrush/-/raw/master/Apk/ColoRush.apk)*


A little game made with unity. You're a ball and must have the right color to get over some barriers.<br>
There are five different modes. In three of them you have to choose the right color to get over the barrier with three buttons (red, yellow, blue).
In the advanced modes you can combinate the colors to create new colors like green or orange.<br>
In the other two modes you have a color, which you cannot change, and have to change the row to get over the barrier with the right color.<br>
In many modes you can collect gems, with which you can buy additional skins or backgrounds in the gem shop.<br>
Have Fun!

<img src="https://gitlab.com/colorush/colorrush/-/raw/master/Screenshots/colorushboss1.png"
      height="600">
<img src="https://gitlab.com/colorush/colorrush/-/raw/master/Screenshots/colorushchoosing2.png"
      height="600">
<img src="https://gitlab.com/colorush/colorrush/-/raw/master/Screenshots/colorushlevel1.png"
      height="600">
<img src="https://gitlab.com/colorush/colorrush/-/raw/master/Screenshots/colorushmenu.png"
      height="600">
      
*Made by <a href="https://github.com/smichel17">smichel17</a> from <a href="https://github.com/LibreShift/red-moon">LibreShift/red-moon</a>
